#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "books.h"
#include "user.h"
#include "Datastructure.h"

int main ()
{
  int option;
  int response;

  // To keep looping the main option menu
  while(1)
  {
    printf("\nWelcome to the Library\n");
    printf("1. Student log in\n");
    printf("2. Librarian log in\n");
    printf("3. New Student\n");
    printf("4. Quit\n");
    printf ("Please enter your choice(1-4):");
    scanf ("%i", &option);

    if (option==1)
    {

      if(logIn(info)==1)
      {
        while(1)
        {
          printf("\n1. Borrow Book\n");
          printf("2. Return a book\n");
          printf("3. List books\n");
          printf("4. Search for a book\n");
          printf("5. Quit\n");
          printf ("Please enter your choice(1-5):");
          scanf ("%i", &response);


          if (response==1)
          {
            borrowBook(info);
          }

          if (response==2)
          {
            returnBook(info);
          }

          if (response==3)
          {
            listBooks(info);
          }

          if (response==4)
          {
            searchBook(info);
          }

          if (response==5)
          {
            exit(0);
          }
        }
      }

    }

    if(option==2)
    {
      if(libLog(info)==1)
      {
        while(1)
        {
          printf("\n1. Add Book\n");
          printf("2. Remove a book\n");
          printf("3. List students\n");
          printf("4. Remove student\n");
          printf("5. List books\n");
          printf("6. List borrowed books\n");
          printf("7. Quit\n");
          printf ("Please enter your choice(1-7):");
          scanf ("%i", &response);


          if (response==1)
          {
            addBook(info);
          }

          if (response==2)
          {
            removeBook(info);
          }

          if (response==3)
          {
            listStudents(info);
          }

          if (response==4)
          {
            removeStudent(info);
          }

          if (response==5)
          {
            listBooks(info);
          }

          if (response==6)
          {
            listbBooks(info);
          }

          if(response==7)
          {
            exit(0);
          }
        }

      }
    }

    if (option==3)
    {
      addStudent(info);
    }

    if (option==4)
    {
      exit(0);
    }


  }

  return 0;
}
