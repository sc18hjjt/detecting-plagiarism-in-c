#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Datastructure.h"


FILE *fp;
FILE *fTmp;
FILE *fthird;

// To add a book to the library
void addBook(struct LIB info)
{
  // Gather information regarding the book
  printf ("Enter the book first name (no spaces allowed): ");
  scanf(" %s", info.bname);

  printf ("Enter the book second name (no spaces allowed): ");
  scanf(" %s", info.bname2);

  printf("Enter the author's first name: " );
  scanf("%s", info.author);

  printf("Enter the author's second name: " );
  scanf("%s", info.author2);

  // Write the infomation into the book file
  fp=fopen ("book.txt","a");
  fprintf(fp,"%s %s %s %s\n",info.bname, info.bname2 , info.author, info.author2);
  fclose(fp);
}


void listBooks (struct LIB info )
{

  fp=fopen("book.txt","r+");

  // If the file is not empty
  if(fp != NULL)
  {
    int i=1;

    // Scans and display infomation regarding books
    while(fscanf(fp,"%s %s %s %s\n",info.bname, info.bname2 , info.author, info.author2)!=EOF)
    {
      printf("%d) %s  %s | %s  %s\n",i,info.bname, info.bname2 , info.author, info.author2);
      i++;
    }
  }


  if (fp==NULL)
  {
    printf(" No Books in the Library");
  }

  fclose(fp);
}

// Same as list book function but instead shows borrowed books
void listbBooks (struct LIB info )
{

  fp=fopen("lend.txt","r+");

  if(fp != NULL)
  {
    int i=1;
    while(fscanf(fp,"%s %s %s %s\n",info.bname, info.bname2 , info.author, info.author2)!=EOF)
    {
      printf("%d) %s  %s | %s  %s\n",i,info.bname, info.bname2 , info.author, info.author2);
      i++;
    }
  }


  if (fp==NULL)
  {
    printf(" All Books in the Library");
  }

  fclose(fp);
}

void searchBook(struct LIB info)
{
  char buffer[1000];
  char sBook[25];
  char sBook2[25];
  char aname[25];
  char aname2[25];
  char word[1000];
  char word2[1000];
  char word3[1000];
  char word4[1000];

  // Reads information about the book
  printf("Enter the book first name(Case Sensitive): ");
  scanf("%s", sBook);

  printf("Enter the book second name(Case Sensitive): ");
  scanf("%s", sBook2);

  printf("Enter book author's first name(Case Sensitive): ");
  scanf("%s", aname);

  printf("Enter book author's second name(Case Sensitive): ");
  scanf("%s", aname2);

  fp  = fopen("book.txt", "r");
  int found=0;
  // Opens the library file and checks whether it exists
  while(fscanf(fp, "%s %s %s %s", word, word2, word3, word4) != EOF)
  {
    // compares each word one by word to avoid books with similar names being found
    if(strcmp(word, sBook) == 0)
    {
      if (strcmp(word2, sBook2)==0)
      {
        if (strcmp(word3, aname)==0)
        {
          if (strcmp(word4, aname2)==0)
          {
            printf("\nBook is available in library\n");
            found=1;
          }
        }
      }
    }
  }

  // Let the user know when a book is not found
  if (found==0)
  {
    printf("\nBook is not found or is unavailable\n");
  }

  fclose(fp);
}

void borrowBook (struct LIB info)
{
  int num;
  char buffer[1000];
  int count = 1;

  // Opens all required files
  fp  = fopen("book.txt", "r");
  fTmp = fopen("tmp.txt", "w+");
  fthird=fopen ("lend.txt","a");

  // Display all books
  int i=1;
  while(fscanf(fp,"%s %s %s %s\n",info.bname, info.bname2 , info.author, info.author2)!=EOF)
  {
    printf("%d) %s  %s | %s  %s\n",i,info.bname, info.bname2 , info.author, info.author2);
    i++;
  }

  // Reads which book number the user wants to borrow
  printf("Please enter the Book number you want to borrow:");
  scanf("%d", &num);

  // To start from the start of the file 
  rewind(fp);

  // if the number exists
  if (i>num)
  {
    while ((fgets(buffer, 1000, fp)) != NULL)
    {
      // Put every line but the line with the book the user wants into a temporary file
      if (num != count)
      {
        fputs(buffer, fTmp);
      }

      // Put the line with the book the user wants into a borrowed book file
      if (num==count)
      {
        fputs(buffer,fthird);
      }

      // Increase the line number
      count++;
    }


    fclose(fp);
    fclose(fTmp);

    remove("book.txt");
    rename("tmp.txt", "book.txt");


    fclose(fthird);

    printf("Book borrowed");
  }

  // if the number does not exists
  if (i<=num)
  {
    printf("Enter a proper book number");
  }

}

// same concept as borrow book function
void returnBook (struct LIB S)
{
  int num;
  char buffer[1000];
  int count = 1;

  // Files are different compared to borrow book function
  fp  = fopen("lend.txt", "r");
  fTmp = fopen("tmp.txt", "w+");
  fthird=fopen ("book.txt","a");

  int i=1;
  while(fscanf(fp,"%s %s %s %s\n",info.bname, info.bname2 , info.author, info.author2)!=EOF)
  {
    printf("%d) %s  %s | %s  %s\n",i,info.bname, info.bname2 , info.author, info.author2);
    i++;
  }

  printf("Please enter the Book number you want to return:");
  scanf("%d", &num);

  rewind(fp);

  if (i>num)
  {
    while ((fgets(buffer, 1000, fp)) != NULL)
    {
      if (num != count)
      {
        fputs(buffer, fTmp);
      }

      if (num==count)
      {
        fputs(buffer,fthird);
      }

      count++;
    }


    fclose(fp);
    fclose(fTmp);

    remove("lend.txt");
    rename("tmp.txt", "lend.txt");


    fclose(fthird);

    printf("Book returned");
  }

  if (i<=num)
  {
    printf("Enter a proper book number");
  }


}

//similar to that of borrow book function
void removeBook (struct LIB info)
{
  int num;
  char buffer[1000];
  int count = 1;

  // Does not require a third file as book is permanent deleted
  fp  = fopen("book.txt", "r");
  fTmp = fopen("tmp.txt", "w+");


  int i=1;
  while(fscanf(fp,"%s %s %s %s\n",info.bname, info.bname2 , info.author, info.author2)!=EOF)
  {
    printf("%d) %s  %s | %s  %s\n",i,info.bname, info.bname2 , info.author, info.author2);
    i++;
  }

  printf("Please enter the Book number you want to remove:");
  scanf("%d", &num);

  rewind(fp);

  if (i>num)
  {
    while ((fgets(buffer, 1000, fp)) != NULL)
    {

      if (num != count)
      {
        fputs(buffer, fTmp);
      }

      count++;
    }


    fclose(fp);
    fclose(fTmp);

    remove("book.txt");
    rename("tmp.txt", "book.txt");

    printf("Book removed\n");
  }

  if (i<=num)
  {
    printf("Enter a proper book number");
  }

}
